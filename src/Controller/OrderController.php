<?php

namespace App\Controller;

use DateTime;
use App\Entity\Order;
use App\Entity\Invoice;
use App\Entity\Product;
use App\Form\OrderType;
use App\Entity\OrderItem;
use App\Form\InvoiceType;
use App\Repository\OrderRepository;
use App\Repository\OrderItemRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

#[Route('/order')]
class OrderController extends AbstractController
{
    #[Route('/add-product/{id}', name: 'app_order_add_product', methods: ['GET'])]
    public function add_product(Product $product, ManagerRegistry $doctrine, OrderItemRepository $orderItemRepository): Response
    {
        $order = $this->getUser()->getCurrentOrder();
        $entityManager = $doctrine->getManager();

        if ($order === null) {
            $order = new Order();
            $order->setDate(new \DateTime('now'));
            $order->setUser($this->getUser());
            $entityManager->persist($order);
        }

        $orderItems = $order->getOrderItems();
        $orderItem = null;

        foreach ($orderItems as $oi) {
            
            if ($oi->getProduct()->getId() == $product->getId()) {
                $orderItem = $oi;
            }
        }

        if ($orderItem == null) {
            $orderItem = new OrderItem();
            $orderItem->setOrder($order);
            $orderItem->setProduct($product);
            $orderItem->setQuantity(0);
        }

        $orderItem->setQuantity($orderItem->getQuantity() + 1);
        
        $entityManager->persist($orderItem);
        $entityManager->flush();

        return $this->redirectToRoute('app_home');
    }

    #[Route('/remove-product/{id}', name: 'app_order_remove_product', methods: ['GET'])]
    public function remove_product(OrderItem $orderItem, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        
        if ($orderItem->getOrder()->getUser()->getId() == $this->getUser()->getId()) {
            $entityManager->remove($orderItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_informations');
    }

    #[Route('/list', name: 'app_order_index', methods: ['GET', 'POST'])]
    public function index(ManagerRegistry $doctrine, OrderRepository $orderRepository, Request $request, SluggerInterface $slugger): Response
    {
        $entityManager = $doctrine->getManager();
        $orders = $orderRepository->findAll();

        $invoice = new Invoice();
        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $invoiceFile = $form->get('invoice')->getData();
            $orderId = $form->get('order')->getData();
            $order = $orderRepository->findOneBy(['id' => $orderId]);

            if ($invoiceFile) {
                $originalFilename = pathinfo($invoiceFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$invoiceFile->guessExtension();
                $invoice->setFilename($newFilename);

                try {
                    $invoiceFile->move(
                        $this->getParameter('uploads_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $order->addInvoice($invoice);
                $entityManager->persist($invoice);
                $entityManager->persist($order);
                $entityManager->flush();
            }
        }

        return $this->getUser() && $this->getUser()->isAdmin() ? $this->render('order/index.html.twig', [
            'orders' => $orders,
            'form' => $form->createview(),
        ]) : $this->redirectToRoute('app_home');
    }

    #[Route('/remove-invoice/{id}', name: 'app_order_remove_invoice', methods: ['GET', 'POST'])]
    public function new(Invoice $invoice, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $order = $invoice->getTheOrder();
        $order->removeInvoice($invoice);

        $entityManager->persist($order);
        $entityManager->remove($invoice);
        $entityManager->flush();
        
        if (file_exists($this->getParameter('uploads_directory') . $invoice->getFilename())) {
            unlink($this->getParameter('uploads_directory') . $invoice->getFilename());
        }
        
        return $this->redirectToRoute('app_order_index');
    }

    // #[Route('/{id}', name: 'app_order_show', methods: ['GET'])]
    // public function show(Order $order): Response
    // {
    //     return $this->render('order/show.html.twig', [
    //         'order' => $order,
    //     ]);
    // }

    // #[Route('/{id}/edit', name: 'app_order_edit', methods: ['GET', 'POST'])]
    // public function edit(Request $request, Order $order, OrderRepository $orderRepository): Response
    // {
    //     $form = $this->createForm(OrderType::class, $order);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $orderRepository->add($order);
    //         return $this->redirectToRoute('app_order_index', [], Response::HTTP_SEE_OTHER);
    //     }

    //     return $this->renderForm('order/edit.html.twig', [
    //         'order' => $order,
    //         'form' => $form,
    //     ]);
    // }

    // #[Route('/{id}', name: 'app_order_delete', methods: ['POST'])]
    // public function delete(Request $request, Order $order, OrderRepository $orderRepository): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
    //         $orderRepository->remove($order);
    //     }

    //     return $this->redirectToRoute('app_order_index', [], Response::HTTP_SEE_OTHER);
    // }
}
