<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // admin
        $user = new User();
        $user->setRoles(["ROLE_USER", "ROLE_ADMIN"]);
        $user->setEmail('admin@gmail.com');
        $user->setFirstname('Étienne');
        $user->setLastname('De Crécy');
        $user->setPassword($this->hasher->hashPassword($user, 'password'));
        $manager->persist($user);


        // random user
        $user = new User();
        $user->setRoles(["ROLE_USER"]);
        $user->setEmail('test@gmail.com');
        $user->setFirstname('Delphine');
        $user->setLastname('Lacroix');
        $user->setPassword($this->hasher->hashPassword($user, 'password'));
        $manager->persist($user);
        

        $productsData = [
            [
                'name' => 'Lorem',
                'picture' => 'https://images.unsplash.com/photo-1598764557991-b9f211b73b81?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1335',
                'price' => '20',
                'picture' => 'igor-son-FV_PxCqgtwc-unsplash.jpg'
            ],
            [
                'name' => 'Lorem 2',
                'picture' => 'https://images.unsplash.com/photo-1598764557991-b9f211b73b81?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1335',
                'price' => '25',
                'picture' => 'scott-webb-oRWRlTgBrPo-unsplash.jpg'
            ],
        ];

        foreach ($productsData as $data) {
            $product = new Product();
            $product->setName($data['name']);
            $product->setPicture($data['picture']);
            $product->setPrice($data['price']);

            $manager->persist($product);
        }

        $manager->flush();
    }
}
