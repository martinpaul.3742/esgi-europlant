Delphine Lacroix / Lucas Burlot / Martin Paul

## Commandes à lancer
- php bin/console d:d:c
- php bin/console d:m:m
- php bin/console d:f:l

## Créé par défaut
- un administrateur
    - email : admin@gmail.com
    - mot de passe : password
- un utilisateur
    - email : test@gmail.com
    - mot de passe : password
